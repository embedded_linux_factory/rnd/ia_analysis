# AI Analysis Project

This project has been realized by Mathieu Ménard, work-study student at [Technology & Strategy](https://www.technologyandstrategy.com/).

The aim of the project is to integrate video recognition, such as object or person detection, on an i.MX 8 target from [TechNexion](https://www.technexion.com/) – the [PICO-PI-IMX8M](https://developer.technexion.com/docs/pico-imx8m) – using the [Yocto Project](https://www.yoctoproject.org/).


## Description

As part of the development of an autonomous all-terrain agricultural robot named KIPP, Technology & Strategy wants to integrate video analysis for object and person detection on an i.MX 8 target with a camera.

The main objectives of that project are:
- From an existing TechNexion’s Yocto image, integrate video recognition for object / person detection
- Measure the performance of video recognition according to embedded constraints (response time, CPU workload...)
- Investigate the possibility of having the same features on Android
- Integrate the AI components into an Innolab ELF image


## Installation

### Hardware

For hardware installation, please refer to the Quickstart Guide provided with the board and available [here](https://www.technexion.com/wp-content/uploads/2021/01/qig-pico-pi-imx8m.pdf).

You can find information about the hardware [here](https://www.technexion.com/products/system-on-modules/evk/pico-pi-imx8m/).

[About PICO-IMX8M](https://developer.technexion.com/docs/pico-imx8m)


### Software

Many tutorials and information are available on the [TechNexion's Developer Portal](https://developer.technexion.com/).


### Building an Image with Yocto

To build an image for the PICO-IMX8M with Yocto, you can refer to [this link](https://developer.technexion.com/docs/building-an-image-with-yocto).

Here, you will learn how to download the TechNexion's sources, build the associated Yocto image, flash the PICO-IMX8M eval-board with this image and use the board (add your own layers and recipes, use the OV5645 camera...).


#### Install required packages

```bash
# First update the host package list
sudo apt update

# Install necessary packages
sudo apt install gawk wget git git-core diffstat unzip texinfo gcc-multilib build-essential \
chrpath socat cpio python python3 python3-pip python3-pexpect \
python3-git python3-jinja2 libegl1-mesa pylint3 rsync bc bison \
xz-utils debianutils iputils-ping libsdl1.2-dev xterm \
language-pack-en coreutils texi2html file docbook-utils \
python-pysqlite2 help2man desktop-file-utils \
libgl1-mesa-dev libglu1-mesa-dev mercurial autoconf automake \
groff curl lzop asciidoc u-boot-tools libreoffice-writer \
sshpass ssh-askpass zip xz-utils kpartx vim screen
```


#### Install the Google's Repo tool

```bash
mkdir ~/bin

curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo

chmod a+x ~/bin/repo

export PATH=${PATH}:~/bin
```


#### Download the BSP source

```bash
mkdir <project_name>

cd <project_name>
```

Go to [this link](https://github.com/TechNexion/tn-imx-yocto-manifest) and choose the release you want to work with.

In the associated README, copy-paste the line starting with `repo init` to initialize the repositories.

e.g. if you want to work with the current latest stable release of Kirkstone:
```bash
repo init -u https://github.com/TechNexion/tn-imx-yocto-manifest.git -b kirkstone_5.15.y-stable -m imx-5.15.71-2.2.2.xml
```


#### Build OS image for TechNexion i.MX8M platform

Refer to the [README](https://github.com/TechNexion/tn-imx-yocto-manifest) mentionned above to understand the different variables used.

For PICO-IMX8M (based on pico-imx8mq processor):
```bash
MACHINE=pico-imx8mq DISTRO=fsl-imx-wayland BASEBOARD=pi-8m WIFI_FIRMWARE=y DISPLAY=mipi5 source tn-setup-release.sh -b build-xwayland-pico-imx8mq

bitbake imx-image-full
```


### Image Deployment (flash the board)

The PICO-IMX8MQ must be flashed via e.MMC.

When the build process completes, the generated release image is in the directory `<build_directory>/tmp/deploy/images/<machine_name>`, e.g. for the PICO-IMX8MQ:
```bash
build-xwayland-pico-imx8mq/tmp/deploy/images/pico-imx8mq/
```

If you are working on the T&S build server, copy the `<image>.wic.bz2` image from your local environment using `scp`:
```bash
scp <username>@<build.server.ip.address>:<path/to/image>.wic.bz2 <path/to/local/folder>
```


#### Hardware configuration

First of all, you need to power off the board and set it to the [Serial Download Mode configuration](https://developer.technexion.com/docs/boot-configurations-of-pico-pi-imx8m-mini#boot-into-serial-download-mode) (same configuration as the PICO-IMX8M-MINI shown), also available [here](https://developer.technexion.com/docs/pico-evaluation-kit-boot-mode-settings#picoimx8m-and-picoimx8mmini).

Then you can connect the USB-C to USB-A cable from the board to your computer.

If you want to follow up the console output while flashing or using the board, you can download and use the `minicom` tool (some configuration needed, see below), or the `screen` program.


#### Flash e.MMC using UUU

The UUU (Universal Update Utility) tool is used to flash the PICO-IMX8MQ's e.MMC.

Download the UUU tool [here](https://download.technexion.com/development_resources/development_tools/installer/imx-mfg-uuu-tool.zip).

Install required packages for running mfgtool (uuu):
```bash
sudo apt install libusb-1.0.0-dev libzip-dev libbz2-dev
```

Flash e.MMC:
```bash
cd imx-mfg-uuu-tool

sudo ./uuu/linux64/uuu -b emmc_img imx8mq/pico-imx8mq/pico-imx8mq-flash.bin <path/to/image/to/flash>/<image_name>.wic.bz2/*
```


#### Flash e.MMC using `bmaptool`

You can also flash the PICO-IMX8MQ's e.MMC using `bmaptool` (not tested yet).

This command will flash image of PICO-IMX8MQ to `/dev/sdj`:
```bash
bmaptool copy --bmap imx-image-full-pico-imx8mq.rootfs.wic.map imx-image-full-pico-imx8mq.rootfs.wic.bz2
```


### Boot from e.MMC

Once the download/flashing process is complete, power off the board by disconnecting the USB-C cable.

Remove the jumpers from the board to boot from e.MMC (or put them in the [e.MMC Boot Mode](https://developer.technexion.com/docs/boot-configurations-of-pico-pi-imx8m-mini#boot-from-emmc), also available [here](https://developer.technexion.com/docs/pico-evaluation-kit-boot-mode-settings#picoimx8m-and-picoimx8mmini)).

You can now connect the board to the mains using a USB-C to USB-C cable and a suitable adapter, and follow the boot process and Kernel installation through `minicom` or `screen` (see further).\
You can choose to connect the power cable to your computer, but please ensure that it can deliver enough power.


## Usage

After checking that the base image works, you can start manipulating the board.


### Configure and use Minicom

Install the `minicom` package:
```bash
sudo apt install minicom
```

Setup ```minicom```:
```bash
sudo minicom -s
```

Enter `Serial port setup` by presseing the "Enter" key.
- Press "A" on your keyboard and replace the current `Serial Device` with `/dev/ttyUSB0`
- Check that the E section (`Bps/Par/Bits`) is set to `115200 8N1`, otherwise press "E" and set it to this value
- Press "F" on your keyboard to switch the `Hardware Flow Control` to `No`

Press the "Enter" key once to go back to the `minicom` configuration menu.

Go down to `Save setup as dfl` and press "Enter" to save the current configuration as default.

Go down to `Exit from Minicom` and press "Enter".

Now you only need to do
```bash
sudo minicom
```
to get the board's serial output and communicate with the board.

Once you can enter `minicom` and that the boot process is complete, press "Enter" and log in as `root` (no password).

You should now be able to use the board's terminal!


### Connect to the board via SSH

First of, find the right IP address to connect to the board (use `ifconfig` or `ip address` in `minicom` or `screen`).

By default, the board uses a dynamic IP address, so you will have to setup a fix IP address if you want to make things easier.

In `minicom`, go to the board's `/etc/systemd/network/` folder and create a new file titled `20-wired.network`.
In this file, write:
```bash
[Match]
Name=eth0

[Network]
Address=<your_IP_address>/24
```

Save the file and reboot the board.

You should now be able to do `ssh root@<your_IP_address>` after connecting the Ethernet cable between the board and your PC.

Please note that you might need to configure your Linux VM settings in VirtualBox, and also your IP settings according to your Windows IP settings, in order to enable both Wi-Fi and SSH connections.

You can also add this file directly to the board's file system with Yocto. Read the next section of this README and find the example in the `develop` branch.


### Creating a layer

You can add your own features to the image by creating new layers and recipes in the Yocto environment's sources.\
You will find some examples (HelloWorlds and others) in the `develop` branch.\
Feel free to use them and to create your own!


### Patching files

If you want to modify any of the files in the build directory, you will have to create a patch and include it to your custom layer.\
The Yocto Project has made it very simple thanks to its tool named `quilt`.

First of all, you have to create your own layer (see section above).\
You want to modify the kernel, so you need to create folders named `recipes-kernel/linux` within this layer.\
Inside this `linux` folder, create a file titled `<kernel-name>_%.bbappend` and a folder called `files`.

Then, you need to source your environment:
```bash
MACHINE=pico-imx8mq DISTRO=fsl-imx-wayland BASEBOARD=pi-8m WIFI_FIRMWARE=y DISPLAY=mipi5 source tn-setup-release.sh -b build-xwayland-pico-imx8mq
```

Once this is done, you should have been driven to the build directory. Launch the following command:
```bash
bitbake virtual/kernel -c devshell
```
This will bring you to the `tmp/work-shared` folder, where you can patch files.

Create a new patch:
```bash
quilt new <patch_name>.patch
```

Add the file you want to modify to the patch:
```bash
quilt add <path/to/file/to/be/patched>
```

Modify the file as you wish.

Refresh the patch to take the modifications into account:
```bash
quilt refresh
```

Your patch is now ready to be used. To do so, copy the patch to the `recipes-kernel/linux/files` directory that you created earlier:
```bash
cp patches/<patch_name>.patch <path/to/your/environment>/sources/meta-<your_layer>/recipes-kernel/linux/files/
```

Finally, in order for Yocto to detect and use your patch, write this in the `recipes-kernel/linux/<kernel-name>_%.bbappend` file within your layer*:
```bash
FILESEXTRAPATHS:prepend := "${THISDIR}/files:"


SRC_URI += " \
        file://<patch_name>.patch \
        "
```
*Be aware that the syntax might be slightly different according to the Yocto version you use.


### Using the OV5645 MIPI-CSI2 camera module

The development kit is also provided with a camera – the [OV5645](https://www.technexion.com/product/cam-ov5645/).\
See [installation guide](https://developer.technexion.com/docs/cam-ov5645-installation).

Please note that the OV5645 camera is not supported anymore by TechNexion on the latest releases. I've tried many things to make it work with a native Kirkstone environment, going through drivers, devicetree and kernel debug attempts, but without concrete results.

I started from the observation that the camera was working perfectly with the old Zeus releases. The issue was that these versions did not support AI components and NPU management.\
But as soon as I switched to more recent versions (from Hardknott), it turned out that the camera was completely bugged. I was unable to use whatever video solution (gstreamer, OpenCV, v4l2...), which isn't very practical for object detection...

However, after a few weeks of debugging, I managed to put the Zeus kernel (the one that works with the OV5645 camera) on the Kirkstone layers and distro.\
You can find my final contribution on the `develop` branch.

You can use `gstreamer` to stream the camera content on the 5'' touchscreen display: refer to [Testing the OV5645 MIPI CSI2 camera module](https://developer.technexion.com/docs/testing-the-ov5645-mipi-csi2-camera-module).

Before using the camera, you need to let the system know that the camera peripheral should be activated and used.\
To to so, open a `minicom`, `screen` or equivalent console, start the board and stop the boot process by tapping any key on the keyboard.\
You should enter "u-boot". Type the following commands:
```bash
setenv dtoverlay ov5645

saveenv
```

Then reboot the board.

Once the board has booted and that you are logged in as root, you can use gstreamer.

```bash
gst-launch-1.0 v4l2src io-mode=3 device=/dev/video0 ! video/x-raw,width=1280,height=720 ! waylandsink
```

Or to display on the entire screen:
```bash
gst-launch-1.0 v4l2src device=/dev/video0 ! video/x-raw,width=640,height=480,framerate=30/1 ! waylandsink window-width=1280 window-height=720
```

For documentation and troubleshooting purposes, you can also check the [PICO-IMX8M-MINI TEVI Camera Usage Guide](https://developer.technexion.com/docs/tevi-cam-on-pico-imx8m-mini).


### Enabling Wi-Fi connection on the board

You can watch [this video](https://www.youtube.com/watch?app=desktop&v=zZygEr8K6cQ&trk) (timecode 1:41 to 4:16) or follow the same tutorial that is given below to connect your board to the Wi-Fi.

Once on the board's console (`minicom`, `screen` or `ssh`):

```bash
connmanctl
```

Now you should be in the connmanctl console (`connmanctl>`).

```bash
scan wifi
```

Wait for the scan to complete.

```bash
agent on

services
```

The last command will list the Wi-Fi connections available. Choose the one you want to connect to and copy the code starting with `wifi_001f` in the right column.

```bash
connect <paste_the_code_you_just_copied>
```

This will ask you to enter the Wi-Fi passphrase. Enter it and wait for the connection to be established.

```bash
quit
```

You should now be back to the root command terminal.

You can execute a command like `ping www.google.com` to check that you receive answers from the internet.

If so, then you are connected!

You might need to disable the ethernet connection (```disconnect <eth...>``` within `connmanctl`) for the Wi-Fi to work. (Don't worry, it won't crash your SSH connection).


### Object Detection and Face Recognition

Regarding the main goals of the project, I chose to start from existing sources.

I went through many different attempts using YOLO, TensorFlow (Lite), eIQ, face-recognition...

You can find most of them in the `develop` branch.

Please refer to my Internship Report to better understand the work process I followed.

Regarding Object Detection, the best results I got were those using the TF-Lite detection framework.

Regarding Face-Recognition, the `face-recognition` library and scripts work well, especially without graphical display.


## Project status

The project took place from feb. 2024 to aug. 2024. It may now be extended by whom it may concern.


## Project sustainability

### Deliverables

This README is the main technical documentation for the project.

The source codes are all available in the `develop` branch.

Finally, my Internship Report and Presentation Support are provided here and in the [Sharepoint](https://technologyandstrategy0.sharepoint.com/sites/LinuxandOSS/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FLinuxandOSS%2FShared%20Documents%2FRnD%2FCIR%2F2024%2FAI%20Analysis%20%2D%20Mathieu%20MENARD&viewid=33989372%2Dfe6d%2D43db%2D8b0d%2Daa0e13f119d5).


### What next?

Here are some ideas for continuing the project (what I haven't had time to do):
- Create or retrieve a Waste dataset and train a TensorFlow model on it;
- Develop your own TensorFlow Lite model, using the [TensorFlow Lite Model Maker](https://www.tensorflow.org/lite/models/modify/model_maker) and following [this turorial](https://www.tensorflow.org/lite/models/modify/model_maker/object_detection) for instance;
- Improve results (response delay, latency, number of Frames Per Second, CPU load...):
- Search how to be sure to use the board's GPU (Graphics Processing Unit) and NPU (Neural Processing Unit);
- Run all the sources in a Yocto environment (following the layer creation tutorial above) in order to create an image containing everything needed;
- Integrate the AI components developed in an ELF Innolab image developed by Technology & Strategy;
- Study the possibility of having the same functionality on Android (state of the art).
